//RECTIFICAR UEYE CAMERA

#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include <opencv2/opencv.hpp>
#include <opencv/highgui.h>
#include <opencv/cv.h>
#include "libuvc/libuvc.h"
#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <string>
#include <ueye.h>
#include <vector>
#include <string>

#include "UEyeOpenCV.hpp"
#include "UEyeOpenCVException.hpp"

using namespace cv;
using namespace std;

bool set_on =1;
bool set_off =0;




int main(int argc, char** argv) {
	
	
	
	UeyeOpencvCam cam1 = UeyeOpencvCam(752,480); 

	cam1.setAutoWhiteBalance(set_on);  
	cam1.setAutoGain(set_on);  
	
	
	     
    
    Mat image; //Imagen Original	
    Mat image_rec; //Imagen Rectificada		
	
    
   
    
	
	Mat camera_matrix, dist_coeffs, new_matrix;
		
	camera_matrix = (Mat_<double>(3,3) << 423.9563, 0., 362.1824, 0., 422.1036, 216.2354, 0., 0., 1. );
	//cout << camera_matrix;
    
         
    dist_coeffs = (Mat_<double>(1,5) << -0.327348625853275, 0.180568640504019, -3.693700252355485e-04,-1.049796681876611e-06, -0.089693338993639); //Entregados por Matlab (K1, K2, P1, P2, K3)
    //cout << dist_coeffs;
    
       
	   
while(1){
        
      
            
							
				image=cam1.getFrame(); //Asigna la matriz de imagen de la cámara a una variable				
				
				undistort(image, image_rec, camera_matrix, dist_coeffs); //Elimina la distorsión radial de la imagen
				
				
				imshow("RECTIFICADA",image_rec);//Muestra la imagen rectificada
                imshow("ORIGINAL",image); //Muestra la imagen original         
				
                if (waitKey(10) >= 0) {
                       break;
                }
                
                
        }
        
        return 0;
}

//To compile: g++ -std=c++0x main.cpp UEyeOpencv.cpp -o test `pkg-config --cflags --libs opencv`-lueye_api
