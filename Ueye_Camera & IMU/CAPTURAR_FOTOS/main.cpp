//CAPTURA DE FOTOS CON UEYE CAMERA Y TOMAR DATOS DE LA IMU LIVE

#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include <opencv2/opencv.hpp>
#include <opencv/highgui.h>
#include <opencv/cv.h>
#include "libuvc/libuvc.h"
#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <string>
#include <ueye.h>
#include <chrono>

#include <fstream>

#include <time.h>

#include "UEyeOpenCV.hpp"
#include "UEyeOpenCVException.hpp"

#include <map>        
#include "myahrs_plus.hpp"  //LIBRERIA IMU

using namespace cv;
using namespace std;

char filename[200];
char key = (char)cv::waitKey(10);
int n = 0;
int TOTAL_FOTOS=2000;//4200;
double tiempo=0;
bool set_on =1;
bool set_off =0;

//Clases y variables globales de la IMU:
using namespace WithRobot;
static const char* DIVIDER = "1";  // 100 Hz
string port_name = "/dev/ttyACM0"; //Dirección de la IMU

void handle_error(const char* error_msg)
{
    fprintf(stderr, "ERROR: %s\n", error_msg);
    exit(1);
}



int main(int argc, char** argv)
{   
	//Archivos para almacenar datos
	ofstream data("/home/tgm1503/Documentos/SLAM/ORB_SLAM2/my_dataset/sequences/00/times.txt");
	ofstream data_imu("IMU.txt");
	
	//IMU:
	
	MyAhrsPlus sensor; 
	SensorData sensor_data;	//Pitch, Roll, Yaw
	
	unsigned int baudrate = 115200;
	int sensor_id = 0;
	
	if(sensor.start(port_name, baudrate) == false) {
        handle_error("start() returns false");
    }
    
    uint32_t sample_count = 0;
	sample_count = sensor.get_sample_count();
	
	//Para trabajar con Ángulos de Euler:
	
	//sensor.cmd_ascii_data_format("RPY");	
	//sensor.cmd_divider(DIVIDER);
	//sensor.cmd_mode("AC") ;
	
	//Para trabajar con Quaternios:
	
	ImuData<float>& imu = sensor_data.imu; //Datos del Sensor
	sensor.cmd_binary_data_format("QUATERNION, IMU");
	sensor.cmd_divider(DIVIDER);
	sensor.cmd_mode("BC") ;
	
	
	
	//CAMARA:	
	
	//Parametros cámara para toma optima de imagenes
     
	   UeyeOpencvCam cam1 = UeyeOpencvCam(752,480);
	   cam1.setAutoWhiteBalance(set_on);  //Balance de blancos
	   cam1.setAutoGain(set_off);  //Autoganancia
			 
       
       Mat image; //Imagen Original
       
       Mat image_rec; //Imagen Rectificada	
       
       Mat camera_matrix, dist_coeffs; //Parametros intrinsecos de la cámara
       
       camera_matrix = (Mat_<double>(3,3) << 423.9563, 0., 362.1824, 0., 422.1036, 216.2354, 0., 0., 1. ); //Entregada por Matlab [fx 0 cx; 0 fy cy, 0 0 1]
		//cout << camera_matrix;
		
		 dist_coeffs = (Mat_<double>(1,5) << -0.327348625853275, 0.180568640504019, -3.693700252355485e-04,-1.049796681876611e-06, -0.089693338993639); //Entregados por Matlab (K1, K2, P1, P2, K3)
		//cout << dist_coeffs;
    
       
       while (n<TOTAL_FOTOS)
       {
		   
        
				//CAMARA
				image=cam1.getFrame(); //Se habilita la cámara para la toma de frames		
				
                               
                undistort(image, image_rec, camera_matrix, dist_coeffs); //Se elimina la distorsión radial de la cámara con sus parametros intrinsecos
                
                
				sprintf(filename,"/home/tgm1503/Documentos/SLAM/ORB_SLAM2/my_dataset/sequences/00/image_0/00%.4d.png",n); //Asigna un nombre a la imagen a guardar y la ruta en donde se va a guardar
				imwrite(filename,image_rec); //Guarda la imagen capturada
				
				 //IMU:
                
               
					// read counter
					sample_count = sensor.get_sample_count();

					// copy sensor data
					sensor.get_data(sensor_data);

					// EULER ANGLES:
					
					//EulerAngle& e = sensor_data.euler_angle;
					//printf("%04d) EulerAngle (roll = %.2f, pitch = %.2f, yaw = %.2f)\n", sample_count, e.roll, e.pitch, e.yaw);
					//data_imu << e.roll;
					//data_imu << endl;
					//data_imu << e.pitch;
					//data_imu << endl; 
					//data_imu << e.yaw;
					//data_imu << endl;
					
					//QUATERNIONS:
					
					Quaternion& q = sensor_data.quaternion; // IMU Quaternion					
					printf("%04d) Quaternion (x = %.2f, y = %.2f, z = %.2f)\n", sample_count, q.x, q.y, q.z);
					data_imu << q.x;
					data_imu << endl;
					data_imu << q.y;
					data_imu << endl; 
					data_imu << q.z;
					data_imu << endl;
					
				
				//TIEMPOS para el times.txt
				
				double t = (double)getTickCount();
				t = ((double)getTickCount() - t)/getTickFrequency();
				tiempo=tiempo + t;
				cout << "t(s)= " << tiempo << endl;
				data << tiempo;
				data << endl;
				
				imshow("IMAGE",image_rec); //imagen rectificada
				
													
				
                    
                    if (waitKey(10) >= 0) {
                     break;
						}
					 	
                
			       
             n=n+1; 
           
        }
        
        
       
        return 0;
}

//To compile: g++ -std=c++0x main.cpp UEyeOpencv.cpp -o test `pkg-config --cflags --libs opencv`-lueye_api

//To compile with IMU: g++ -std=c++0x main.cpp UEyeOpencv.cpp -o test `pkg-config --cflags --libs opencv`-lueye_api -lm -lpthread

//To start the IMU: sudo chmod 777 /dev/ttyACM0 
