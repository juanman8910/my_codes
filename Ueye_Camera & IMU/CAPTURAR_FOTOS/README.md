# UEye Camera to OpenCV

Test application for a capture of a frame with the IDS UEye drivers and passage to OpenCV format.

To compile: g++ -std=c++0x main.cpp UEyeOpencv.cpp -o test `pkg-config --cflags --libs opencv`-lueye_api

To compile with IMU: g++ -std=c++0x main.cpp UEyeOpencv.cpp -o test `pkg-config --cflags --libs opencv`-lueye_api -lm -lpthread

To start the IMU: sudo chmod 777 /dev/ttyACM0

# Requirements:

Libuvc libraries (https://github.com/ktossell/libuvc)

Ueye Libraries and SDK (https://en.ids-imaging.com/download-ueye-lin64.html)

IMU libraries and SDK (https://github.com/withrobot/myAHRS_plus/tree/master/common_cpp)


