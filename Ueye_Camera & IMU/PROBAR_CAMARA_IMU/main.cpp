//PROBAR FUNCIONAMIENTO UEYE CAMERA E IMU myAHRS+

#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include <opencv2/opencv.hpp>
#include <opencv/highgui.h>
#include <opencv/cv.h>
#include "libuvc/libuvc.h"
#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <string>
#include <ueye.h>
#include <vector>
#include <string>

#include "UEyeOpenCV.hpp"
#include "UEyeOpenCVException.hpp"

#include <map>        
#include "myahrs_plus.hpp"  //LIBRERIA IMU

using namespace cv;
using namespace std;

//Clases y variables globales de la IMU:
using namespace WithRobot;
static const char* DIVIDER = "1";  // 100 Hz
string port_name = "/dev/ttyACM0"; //Dirección de la IMU


bool set_on =1;
bool set_off =0;

void handle_error(const char* error_msg)
{
    fprintf(stderr, "ERROR: %s\n", error_msg);
    exit(1);
}


int main(int argc, char** argv) {
	
	//IMU:
	
	MyAhrsPlus sensor; 
	SensorData sensor_data;	//x,y,z & Pitch, Roll, Yaw
	
	unsigned int baudrate = 115200;
	int sensor_id = 0;
	
	if(sensor.start(port_name, baudrate) == false) {
        handle_error("start() returns false");
    }
	
	
	
	uint32_t sample_count = 0;
	sample_count = sensor.get_sample_count();
	
	//To work with quaternions:
	
	ImuData<float>& imu = sensor_data.imu; //Datos del Sensor
	sensor.cmd_binary_data_format("QUATERNION, IMU");
	sensor.cmd_divider(DIVIDER);
	sensor.cmd_mode("BC") ;
	
	//To work with euler angles:
	
	//sensor.cmd_ascii_data_format("RPY");
	//sensor.cmd_divider(DIVIDER);
	//sensor.cmd_mode("AC") ;
	
	

	
	
	//CAMARA:
	
	UeyeOpencvCam cam1 = UeyeOpencvCam(752,480); 
	
	//cam1.setAutoWhiteBalance(set_on);  
	//cam1.setAutoGain(set_on);  	     
    
    Mat image; //Imagen Original	
    Mat image_rec; //Imagen Rectificada	
    
	
	Mat camera_matrix, dist_coeffs, new_matrix;
		
	camera_matrix = (Mat_<double>(3,3) << 423.9563, 0., 362.1824, 0., 422.1036, 216.2354, 0., 0., 1. );	
	//cout << camera_matrix;    
         
    dist_coeffs = (Mat_<double>(1,5) << -0.327348625853275, 0.180568640504019, -3.693700252355485e-04,-1.049796681876611e-06, -0.089693338993639); //Entregados por Matlab (K1, K2, P1, P2, K3)
    //cout << dist_coeffs;
    
       
	   
while(1){      
	
	
	
                //CAMARA:
							
				image=cam1.getFrame(); //Asigna la matriz de imagen de la cámara a una variable				
				
				undistort(image, image_rec, camera_matrix, dist_coeffs); //Elimina la distorsión radial de la imagen
				
				
				imshow("RECTIFICADA",image_rec);//Muestra la imagen rectificada
                imshow("ORIGINAL",image); //Muestra la imagen original  
                
                
                //IMU:
                
               
					// read counter
					sample_count = sensor.get_sample_count();

					// copy sensor data
					sensor.get_data(sensor_data);

					//print euler angles or Quaternions:
					
					//EulerAngle& e = sensor_data.euler_angle;
					//printf("%04d) EulerAngle (roll = %.2f, pitch = %.2f, yaw = %.2f)\n", sample_count, e.roll, e.pitch, e.yaw);
					
					Quaternion& q = sensor_data.quaternion; // IMU Quaternion					
					printf("%04d) Quaternion (x = %.2f, y = %.2f, z = %.2f)\n", sample_count, q.x, q.y, q.z);
					
				
                
                	
               			
                if (waitKey(10) >= 0) {
					break;
                }
                
                
        }
        
        return 0;
}

//To compile: g++ -std=c++0x main.cpp UEyeOpencv.cpp -o test `pkg-config --cflags --libs opencv`-lueye_api 

//To compile with IMU: g++ -std=c++0x main.cpp UEyeOpencv.cpp -o test `pkg-config --cflags --libs opencv`-lueye_api -lm -lpthread

//To start the IMU: sudo chmod 777 /dev/ttyACM0 

