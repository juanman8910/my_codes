//my_mono_kitti.cpp

#include<iostream>
#include<algorithm>
#include<fstream>
#include<chrono>
#include<iomanip>
#include<thread>
#include<ueye.h> //LIBRERIA CAMARA
#include<time.h> //LIBRERIA TIEMPOS



//LIBRERIAS ALGORITMO:
#include<opencv2/core/core.hpp>
#include "System.h"

//REQUISITOS PARA FUNCIONAMIENTO DE CAMARA:

#include "UEyeOpenCV.hpp"
#include "UEyeOpenCVException.hpp"


#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include <opencv2/opencv.hpp>
#include <opencv/highgui.h>
#include <opencv/cv.h>
#include <libuvc/libuvc.h>

#include <stdio.h>
#include <unistd.h>
#include <string>
#include <vector>
#include <string>



using namespace cv;
using namespace std;


bool set_on =1;
bool set_off =0;

double tiempo=0;


//void LoadImages(const string &strSequence, vector<string> &vstrImageFilenames,
                //vector<double> &vTimestamps);

int main(int argc, char **argv)
{
	//CAMARA
	   UeyeOpencvCam cam1 = UeyeOpencvCam(752,480);
	   cam1.setAutoWhiteBalance(set_on);  //Balance de blancos
	   cam1.setAutoGain(set_off);  //Autoganancia
			 
       
       Mat image; //Imagen Original
       
       Mat image_rec; //Imagen Rectificada	
       
       Mat camera_matrix, dist_coeffs; //Parametros intrinsecos de la cámara
       
       camera_matrix = (Mat_<double>(3,3) << 423.9563, 0., 362.1824, 0., 422.1036, 216.2354, 0., 0., 1. ); //Entregada por Matlab [fx 0 cx; 0 fy cy, 0 0 1]
		//cout << camera_matrix;
		
		 dist_coeffs = (Mat_<double>(1,5) << -0.327348625853275, 0.180568640504019, -3.693700252355485e-04,-1.049796681876611e-06, -0.089693338993639); //Entregados por Matlab (K1, K2, P1, P2, K3)
		//cout << dist_coeffs;
		
		
		
		//while(1){    
	
	            ////CAMARA:
							
				//image=cam1.getFrame(); //Asigna la matriz de imagen de la cámara a una variable				
				
				//undistort(image, image_rec, camera_matrix, dist_coeffs); //Elimina la distorsión radial de la imagen				
				
				//imshow("RECTIFICADA",image_rec);//Muestra la imagen rectificada
				
				
				//if (waitKey(10) >= 0) {
                     //break;
						//}
					 	
			//}
//}		


		//----------------------------------------------------------------------------------------------------------------//
		
    //How to execute:
    
    if(argc != 3)
    {
        cerr << endl << "Usage: ./my_mono_kitti path_to_vocabulary path_to_settings" << endl;
        return 1;
    }

    // Retrieve paths to images
    //vector<string> vstrImageFilenames;
    //vector<double> vTimestamps;
    //LoadImages(string(argv[3]), vstrImageFilenames, vTimestamps);

    //int nImages = vstrImageFilenames.size();

    // Create SLAM system. It initializes all system threads and gets ready to process frames.
    ORB_SLAM2::System SLAM(argv[1],argv[2],ORB_SLAM2::System::MONOCULAR,true);

    // Vector for tracking time statistics
    vector<float> vTimesTrack;
    //vTimesTrack.resize(nImages);

    cout << endl << "-------" << endl;
    cout << "Start processing sequence ..." << endl;
    //cout << "Images in the sequence: " << nImages << endl << endl;

    // Main loop
    cv::Mat im;
    
    while(1)
    //for(int ni=0; ni<nImages; ni++)
    {
		
		image=cam1.getFrame(); //Se habilita la cámara para la toma de frames		                   
		undistort(image, image_rec, camera_matrix, dist_coeffs); //Se elimina la distorsión radial de la cámara con sus parametros intrinsecos
		
        // Read image from file
        im = image_rec;
        
        double t = (double)getTickCount();
		t = ((double)getTickCount() - t)/getTickFrequency();
		tiempo=tiempo + t;
        
        //double tframe = vTimestamps[ni];
        double tframe = tiempo;

        //if(im.empty())
        //{
          //  cerr << endl << "Failed to load image at: " << vstrImageFilenames[ni] << endl;
            //return 1;
        //}

#ifdef COMPILEDWITHC11
        std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();
#else
        std::chrono::monotonic_clock::time_point t1 = std::chrono::monotonic_clock::now();
#endif

        // Pass the image to the SLAM system
        SLAM.TrackMonocular(im,tframe);

//#ifdef COMPILEDWITHC11
 //       std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();
//#else
  //      std::chrono::monotonic_clock::time_point t2 = std::chrono::monotonic_clock::now();
//#endif

        //double ttrack= std::chrono::duration_cast<std::chrono::duration<double> >(t2 - t1).count();

      //  vTimesTrack[ni]=ttrack;

        // Wait to load the next frame
        //double T=0;
        //if(ni<nImages-1)
          //  T = vTimestamps[ni+1]-tframe;
        //else if(ni>0)
          //  T = tframe-vTimestamps[ni-1];

        //if(ttrack<T)
          //  usleep((T-ttrack)*1e6);
    }

    // Stop all threads
    SLAM.Shutdown();

    // Tracking time statistics
    //sort(vTimesTrack.begin(),vTimesTrack.end());
    //float totaltime = 0;
    //for(int ni=0; ni<nImages; ni++)
    //{
    //    totaltime+=vTimesTrack[ni];
    //}
    //cout << "-------" << endl << endl;
    //cout << "median tracking time: " << vTimesTrack[nImages/2] << endl;
    //cout << "mean tracking time: " << totaltime/nImages << endl;

    // Save camera trajectory
    SLAM.SaveKeyFrameTrajectoryTUM("KeyFrameTrajectory.txt");

    return 0;
}

//void LoadImages(const string &strPathToSequence, vector<string> &vstrImageFilenames, vector<double> &vTimestamps)
//{
    //ifstream fTimes;
    //string strPathTimeFile = strPathToSequence + "/times.txt";
    //fTimes.open(strPathTimeFile.c_str());
    //while(!fTimes.eof())
    //{
        //string s;
        //getline(fTimes,s);
        //if(!s.empty())
        //{
            //stringstream ss;
            //ss << s;
            //double t;
            //ss >> t;
            //vTimestamps.push_back(t);
        //}
    //}

    //string strPrefixLeft = strPathToSequence + "/image_0/";

    //const int nTimes = vTimestamps.size();
    //vstrImageFilenames.resize(nTimes);

    //for(int i=0; i<nTimes; i++)
    //{
        //stringstream ss;
        //ss << setfill('0') << setw(6) << i;
        //vstrImageFilenames[i] = strPrefixLeft + ss.str() + ".png";
    //}
//}
