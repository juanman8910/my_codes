#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include <opencv2/opencv.hpp>
#include <opencv/highgui.h>
#include <opencv/cv.h>
#include "libuvc/libuvc.h"
#include <iostream>
#include <stdio.h>
#include <unistd.h>


using namespace cv;
using namespace std;

char filename[200];
char key = (char)waitKey(10);
int n = 0;
int TOTAL_FOTOS=10;

int main()
{
	cout << "Built with OpenCV " << CV_VERSION << endl;
    Mat image;
    Mat gray_image;
    VideoCapture capture;
    
    capture.open(1); //Determina el puerto de lectura de la cámara
    
    while(n<TOTAL_FOTOS) //Toma el número de fotos en TOTAL_FOTOS
    {
     
    if(capture.isOpened()) //Si la cámara esta funcionando
    {
        cout << "Capture is opened" << endl; //Mensaje en Pantalla
        
       for(;;)
        {
			
			capture >> image; //Permite empezar a capturar imagen desde la cámara
            cvtColor( image, gray_image, CV_BGR2GRAY ); //Pasa la imagen a escala de grises
            
                    
            if(image.empty()) 
            break;
            imshow("IMAGEN", gray_image); //Se observa la imagen de la cámara            
                                       
                                           
            if (key == ' ') //Tomar la imagen con la barra espaciadora 
            break;
            sprintf(filename,"Imagen%.3d.jpg",n); //Asigna un nombre a la imagen a guardar
            imwrite(filename,gray_image); //Guarda la imagen capturada
            
											                      
			if(waitKey(10) >= 0) //Se cierra al presionar cualquier tecla
			break;
			
			
			
			
        }
     n=n+1; //Incrementa el contador
    }
    else
    {   
		cout << "No capture" << endl;
        image = Mat::zeros(480, 640, CV_8UC1);
        imshow("IMAGEN", gray_image);
        waitKey(0);
        
    }
    }
    
    return 0;

}
