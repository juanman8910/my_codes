//CAPTURA DE FOTOS CON UEYE CAMERA

#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include <opencv2/opencv.hpp>
#include <opencv/highgui.h>
#include <opencv/cv.h>
#include "libuvc/libuvc.h"
#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <string>
#include <ueye.h>
#include <chrono>

#include <fstream>

#include <time.h>

#include "UEyeOpenCV.hpp"
#include "UEyeOpenCVException.hpp"

using namespace cv;
using namespace std;

char filename[200];
char key = (char)cv::waitKey(10);
int n = 0;
int TOTAL_FOTOS= 4200;
double tiempo=0;
bool set_on =1;
bool set_off =0;


int main(int argc, char** argv) 
{
	
	ofstream data("times.txt");
	
		//Parametros cámara para toma optima de imagenes
     
       UeyeOpencvCam cam1 = UeyeOpencvCam(752,480);
       cam1.setAutoWhiteBalance(set_on);  //Balance de blancos
       cam1.setAutoGain(set_off);  //Autoganancia
             
       
       Mat image; //Imagen Original
       
       Mat image_rec; //Imagen Rectificada	
       
       Mat camera_matrix, dist_coeffs; //Parametros intrinsecos de la cámara
       
       camera_matrix = (Mat_<double>(3,3) << 423.9563, 0., 362.1824, 0., 422.1036, 216.2354, 0., 0., 1. ); //Entregada por Matlab [fx 0 cx; 0 fy cy, 0 0 1]
		cout << camera_matrix;
		
		 dist_coeffs = (Mat_<double>(1,5) << -0.327348625853275, 0.180568640504019, -3.693700252355485e-04,-1.049796681876611e-06, -0.089693338993639); //Entregados por Matlab (K1, K2, P1, P2, K3)
		cout << dist_coeffs;
    
       
       while (n<TOTAL_FOTOS)
       {
		   
        
					
				image=cam1.getFrame(); //Se habilita la cámara para la toma de frames
							
				
                
                
                undistort(image, image_rec, camera_matrix, dist_coeffs); //Se elimina la distorsión radial de la cámara con sus parametros intrinsecos
                
                
				sprintf(filename,"00%.4d.png",n); //Asigna un nombre a la imagen a guardar
				imwrite(filename,image_rec); //Guarda la imagen capturada
				
				//TIEMPOS para el times.txt
				
				double t = (double)getTickCount();
				t = ((double)getTickCount() - t)/getTickFrequency();
				tiempo=tiempo + t;
				cout << "t(s)= " << tiempo << endl;
				data << tiempo;
				data << endl;
				
				imshow("IMAGE",image_rec); //imagen rectificada
				
													
				
                    
                    if (waitKey(10) >= 0) {
                     break;
						}
					 	
                
			       
             n=n+1; 
           
        }
        
        
       
        return 0;
}

//To compile: g++ -std=c++0x main.cpp UEyeOpencv.cpp -o test `pkg-config --cflags --libs opencv`-lueye_api
